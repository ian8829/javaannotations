package com.annotation.override;

public class Main {
    public static void main(String[] args) {
        Cheese myCheese = new Cheese();
        System.out.println(myCheese);
        int x = 7/0;
        // javac -d out -cp src src/com/annotation/override/Main.java
        // java -cp out com.annotation.override.Main
        // javac -Xlint -d out -cp src src/com/annotation/override/Main.java
    }
}